﻿Option Explicit On
Option Strict Off

Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Text
Imports System.Collections.Generic


Public Class clsDAL

    Public Class clsConstantes
        Public Enum ModoTrabajoDAL
            ModoDebug = 1
            ModoNormal = 2
        End Enum
    End Class

    Private cn As New SqlConnection
    Private strErrores() As String
    Public bolAbrio As Boolean
    Private dsDatos As New DataSet
    Private bolError As Boolean
    Private strGUID As String
    Private pModo As clsConstantes.ModoTrabajoDAL
    Private bolRestablecerConexion As Boolean = False

    Public Sub New()
        bolAbrio = False
        bolError = False
        strGUID = ""
        pModo = clsConstantes.ModoTrabajoDAL.ModoNormal
        Try
            'Actualizado por que el método anterior es obsoleto.
            'Dim strModo As String = System.Configuration.ConfigurationSettings.AppSettings("modoTrabajoDAL")
            Dim strModo As String = System.Configuration.ConfigurationManager.AppSettings("modoTrabajoDAL")
            If UCase(strModo) = "DEBUG" Then
                pModo = clsConstantes.ModoTrabajoDAL.ModoDebug
            End If
        Catch ex As Exception
            'continuar 
        End Try
        ReDim strErrores(0)
    End Sub

    Public Function mAbrirBasedeDatos(ByVal strConexion As String, ByVal EsCifrada As Boolean) As Boolean
        If EsCifrada Then
            Dim datos() As Byte
            ReDim datos(0)
            bolError = False
            ReDim strErrores(0)
            clsEncriptaDesEncripta.Decrypt(clsEncriptaDesEncripta.StringToByteArray(strConexion), datos)
            strConexion = Encoding.UTF8.GetString(datos)
        End If

        If bolAbrio Then
            bolError = True
            strErrores(0) = "Conexion abierta previamente"
            mAbrirBasedeDatos = False
        Else
            Try
                cn.ConnectionString = strConexion
                cn.Open()
                mAbrirBasedeDatos = True
                bolAbrio = True
            Catch ex As Exception
                bolError = True
                ReDim strErrores(1)
                strErrores(0) = ex.Message
                strErrores(1) = ex.ToString
                mAbrirBasedeDatos = False
                bolAbrio = False
            End Try
        End If
        If mAbrirBasedeDatos Then
            strGUID = System.Guid.NewGuid.ToString
        Else
            strGUID = ""
        End If
    End Function

    Public Function mCerrarBasedeDatos() As Boolean
        bolError = False
        strGUID = ""
        ReDim strErrores(0)
        If Not bolAbrio Then
            bolError = True
            strErrores(0) = "Conexion no abierta previamente"
            mCerrarBasedeDatos = False
        Else
            Try
                cn.Close()
                bolAbrio = False
                mCerrarBasedeDatos = True
            Catch ex As Exception
                bolError = True
                ReDim strErrores(1)
                strErrores(0) = ex.Message
                strErrores(1) = ex.ToString
                mCerrarBasedeDatos = False
            End Try
        End If
    End Function

    Public Function mCantidadErrores() As Long
        If Not bolError Then
            mCantidadErrores = 0
        Else
            mCantidadErrores = UBound(strErrores, 1) + 1
        End If
    End Function

    Public Function mDescripcionError(ByVal intIndex As Integer) As String
        If Not bolError Then
            Throw New System.Exception("No hay errores en la coleccion")
        Else
            Try
                mDescripcionError = strErrores(intIndex)
            Catch ex As Exception
                Throw New System.Exception("Indice Invalido para la funcion de recuperacion de la Descripcion del error")
            End Try
        End If
    End Function

    Public Function mRST(ByVal strSQL As String, ByRef parametros() As System.Data.SqlClient.SqlParameter) As Long
        Dim intI As Integer
        Dim DA As SqlDataAdapter
        Dim reader1 As System.Data.SqlClient.SqlDataReader = Nothing
        Dim bolRead1 As Boolean
        Dim Comando1 As New System.Data.SqlClient.SqlCommand
        Dim Comando2 As New System.Data.SqlClient.SqlCommand
        Dim Comando3 As New System.Data.SqlClient.SqlCommand
        Dim strSQLDebug As String
        Dim strMensaje As String
        Dim dsDatos2 As New DataSet
        Dim strEvento As String
        Dim lngTicks1 As Long
        Dim lngTicks2 As Long
        mRST = -999
        bolRead1 = False
        lngTicks1 = Now.Ticks
        bolError = False
        ReDim strErrores(0)
        If Not bolAbrio Then
            bolError = True
            strErrores(0) = "La base de datos no esta abierta"
        Else
            Try
                dsDatos.Clear()
                dsDatos.Tables.Clear()
                Comando1.Connection = cn
                If Not IsNothing(parametros) Then
                    Comando1.Parameters.AddRange(parametros)
                End If
                Comando1.CommandText = strSQL
                Comando1.CommandType = CommandType.StoredProcedure

                DA = New SqlDataAdapter(Comando1)

                DA.Fill(dsDatos)

                bolRead1 = True
                If Not IsNothing(parametros) Then
                    For intI = 1 To parametros.Length
                        Dim p As New System.Data.SqlClient.SqlParameter
                        p = Comando1.Parameters.Item(intI - 1)
                        parametros(intI - 1) = p
                    Next
                End If
                Comando1.Parameters.Clear()
                If Not dsDatos Is Nothing Then
                    If dsDatos.Tables.Count > 0 Then
                        mRST = dsDatos.Tables(0).Rows.Count
                    End If
                End If
                If pModo = clsConstantes.ModoTrabajoDAL.ModoDebug Then
                    If bolRead1 Then
                        DA.Dispose()
                        bolRead1 = False
                    End If
                    strSQLDebug = "select @@trancount as C"
                    Comando2.CommandText = strSQLDebug
                    Comando2.Connection = cn
                    dsDatos2.Clear()
                    dsDatos2.Tables.Clear()
                    dsDatos2.Tables.Add(New DataTable)
                    reader1 = Comando2.ExecuteReader
                    bolRead1 = True
                    dsDatos2.Tables(0).Load(reader1)
                    Comando2.Parameters.Clear()
                    If dsDatos2.Tables(0).Rows(0).Item(0) <> 0 Then
                        mRST = -999
                        bolError = True
                        ReDim strErrores(0)
                        strMensaje = "Trancount <>0 ejecutando:" + strSQL + vbCrLf
                        strMensaje = strMensaje + "FAVOR LLAMAR A SISTEMAS INMEDIATAMENTE"
                        strErrores(0) = strMensaje
                        strEvento = "Error:Trancount RS:" + strSQL.Replace(vbCrLf, "<enter>") + ":Parametros"
                        For intI = 1 To parametros.Length
                            strEvento = strEvento + ":" + parametros(intI - 1).ParameterName + ":" + parametros(intI - 1).Value.ToString + "<enter>"
                        Next
                        strEvento = strEvento + ":Errores"
                        For intI = 0 To UBound(strErrores, 1)
                            strEvento = strEvento + ":" + strErrores(intI).Replace(vbCrLf, "<enter>")
                        Next
                        If bolRead1 Then
                            reader1.Close()
                            bolRead1 = False
                        End If
                        Throw New System.Exception(strMensaje + vbCrLf + strEvento)
                    End If
                End If
            Catch ex As Exception
                bolError = True
                ReDim strErrores(2)
                strErrores(0) = strSQL
                strErrores(1) = ex.Message
                strErrores(2) = ex.ToString
                mRST = -999
                If pModo = clsConstantes.ModoTrabajoDAL.ModoDebug Then
                    If bolRead1 Then
                        reader1.Close()
                        bolRead1 = False
                    End If
                    strSQLDebug = "select @@trancount as C"
                    Comando3.CommandText = strSQLDebug
                    Comando3.Connection = cn
                    dsDatos2.Clear()
                    dsDatos2.Tables.Add(New DataTable)
                    reader1 = Comando3.ExecuteReader
                    bolRead1 = True
                    dsDatos2.Tables(0).Load(reader1)
                    Comando3.Parameters.Clear()
                    If dsDatos2.Tables(0).Rows(0).Item(0) <> 0 Then
                        mRST = -999
                        bolError = True
                        ReDim strErrores(0)
                        strMensaje = "Trancount <>0 ejecutando:" + strSQL + vbCrLf
                        strMensaje = strMensaje + "FAVOR LLAMAR A SISTEMAS INMEDIATAMENTE"
                        strErrores(0) = strMensaje
                        strEvento = "Error:Trancount RS:" + strSQL.Replace(vbCrLf, "<enter>") + ":Parametros"
                        For intI = 1 To parametros.Length
                            strEvento = strEvento + ":" + parametros(intI - 1).ParameterName + ":" + parametros(intI - 1).Value.ToString + "<enter>"
                        Next
                        strEvento = strEvento + ":Errores"
                        For intI = 0 To UBound(strErrores, 1)
                            strEvento = strEvento + ":" + strErrores(intI).Replace(vbCrLf, "<enter>")
                        Next
                        If bolRead1 Then
                            reader1.Close()
                            bolRead1 = False
                        End If
                        Throw New System.Exception(strMensaje + vbCrLf + strEvento)
                    End If
                End If
            End Try
        End If
        If bolRead1 Then
            DA.Dispose()
            bolRead1 = False
        End If
        If pModo = clsConstantes.ModoTrabajoDAL.ModoDebug And bolError Then
            lngTicks2 = Now.Ticks
            strEvento = "Recordset:Error:" + strSQL.Replace(vbCrLf, "<enter>") + ":Parametros"
            For intI = 1 To parametros.Length
                strEvento = strEvento + ":" + parametros(intI - 1).ParameterName + ":" + parametros(intI - 1).Value.ToString + "<enter>"
            Next
            strEvento = strEvento + ":Errores"
            For intI = 0 To UBound(strErrores, 1)
                strEvento = strEvento + ":" + strErrores(intI).Replace(vbCrLf, "<enter>")
            Next
        End If
        If pModo = clsConstantes.ModoTrabajoDAL.ModoDebug And Not bolError Then
            lngTicks2 = Now.Ticks
            strEvento = "Recordset:OK:" + strSQL.Replace(vbCrLf, "<enter>") + ":Parametros"
            For intI = 1 To parametros.Length
                strEvento = strEvento + ":" + parametros(intI - 1).ParameterName + ":" + parametros(intI - 1).Value.ToString + "<enter>"
            Next
        End If
    End Function

    Public Function mDatosRST(ByVal lngRow As Long, ByVal objCol As Object) As Object
        Dim myRow As DataRow
        Dim strMensaje As String
        Dim strEvento As String
        Dim intI As Integer
        bolError = False
        ReDim strErrores(0)
        Try
            myRow = dsDatos.Tables(0).Rows(lngRow)
            mDatosRST = myRow.Item(objCol)
        Catch ex As Exception
            strMensaje = "Indice de fila o columna invalidos para funcion DatosRST"
            bolError = True
            ReDim strErrores(2)
            strErrores(0) = "Fila:" + CStr(lngRow)
            strErrores(1) = "Columna:" + CStr(objCol)
            strErrores(2) = strMensaje
            If pModo = clsConstantes.ModoTrabajoDAL.ModoDebug Then
                strEvento = "Error:mDatosRST:"
                For intI = 0 To UBound(strErrores, 1)
                    strEvento = strEvento + ":" + strErrores(intI).Replace(vbCrLf, "<enter>")
                Next
            End If
            Throw New System.Exception(strMensaje)
        End Try
    End Function

    Public Function mExecute(ByVal strSQL As String, ByRef parametros() As System.Data.SqlClient.SqlParameter, Optional ByVal CommandTimeout As Integer? = Nothing) As Long
        Dim intI As Integer
        Dim reader1 As System.Data.SqlClient.SqlDataReader = Nothing
        Dim bolRead1 As Boolean
        Dim Comando1 As New System.Data.SqlClient.SqlCommand
        Dim Comando2 As New System.Data.SqlClient.SqlCommand
        Dim Comando3 As New System.Data.SqlClient.SqlCommand
        Dim strSQLDebug As String
        Dim strMensaje As String
        Dim dsDatos2 As New DataSet

        If bolRestablecerConexion Then
            Try
                bolRestablecerConexion = False
                mCerrarBasedeDatos()
                mAbrirBasedeDatos(cn.ConnectionString, False)
            Catch ex As Exception
            End Try
        End If

        '    Dim objLogger As New clsLogger
        Dim strEvento As String
        Dim lngTicks1 As Long
        Dim lngTicks2 As Long

        mExecute = -999
        bolRead1 = False
        'falta commands timeout de los tres command
        lngTicks1 = Now.Ticks
        bolError = False
        ReDim strErrores(0)
        If Not bolAbrio Then
            bolError = True
            strErrores(0) = "La base de datos no esta abierta"
            mExecute = -999
        Else
            Try
                Comando1.Connection = cn
                Comando1.Parameters.AddRange(parametros)
                Comando1.CommandText = strSQL
                Comando1.CommandType = CommandType.StoredProcedure
                If CommandTimeout.HasValue Then
                    Comando1.CommandTimeout = CommandTimeout
                End If
                'Recordar que si tiene la variable set nocount on 
                'no devuelve el numero de filas
                mExecute = Comando1.ExecuteNonQuery()
                For intI = 1 To parametros.Length
                    Dim p As New System.Data.SqlClient.SqlParameter
                    p = Comando1.Parameters.Item(intI - 1)
                    parametros(intI - 1) = p
                Next
                Comando1.Parameters.Clear()
                If pModo = clsConstantes.ModoTrabajoDAL.ModoDebug Then
                    strSQLDebug = "select @@trancount as C"
                    Comando2.CommandText = strSQLDebug
                    Comando2.Connection = cn
                    dsDatos2.Clear()
                    dsDatos2.Tables.Add(New DataTable)
                    reader1 = Comando2.ExecuteReader
                    bolRead1 = True
                    dsDatos2.Tables(0).Load(reader1)
                    Comando2.Parameters.Clear()
                    If dsDatos2.Tables(0).Rows(0).Item(0) <> 0 Then
                        mExecute = -999
                        bolError = True
                        ReDim strErrores(0)
                        strMensaje = "Trancount <>0 ejecutando:" + strSQL + vbCrLf
                        strMensaje = strMensaje + "FAVOR LLAMAR A SISTEMAS INMEDIATAMENTE"
                        strErrores(0) = strMensaje
                        strEvento = "Error:Trancount Execute:" + strSQL.Replace(vbCrLf, "<enter>") + ":Parametros"
                        For intI = 1 To parametros.Length
                            strEvento = strEvento + ":" + parametros(intI - 1).ParameterName + ":" + parametros(intI - 1).Value.ToString + "<enter>"
                        Next
                        strEvento = strEvento + ":Errores"
                        For intI = 0 To UBound(strErrores, 1)
                            strEvento = strEvento + ":" + strErrores(intI).Replace(vbCrLf, "<enter>")
                        Next
                        If bolRead1 Then
                            reader1.Close()
                            bolRead1 = False
                        End If
                        Throw New System.Exception(strMensaje + vbCrLf + strEvento)
                    End If
                End If
            Catch ex As Exception
                Try
                    If TryCast(ex, SqlException).Number = 10054 Then 'Problemas de capa de transporte. Se intentará restablecer la conexión.
                        bolRestablecerConexion = True
                        MsgBox("Problemas de transporte. Por favor contacte al equipo de soporte para recibir instrucciones.", MsgBoxStyle.Exclamation)
                    End If
                Catch ex2 As Exception
                End Try
                bolError = True
                ReDim strErrores(2)
                strErrores(0) = strSQL
                strErrores(1) = ex.Message
                strErrores(2) = ex.ToString
                mExecute = -999
                If pModo = clsConstantes.ModoTrabajoDAL.ModoDebug Then
                    strSQLDebug = "select @@trancount as C"
                    If bolRead1 Then
                        reader1.Close()
                        bolRead1 = False
                    End If
                    Comando3.CommandText = strSQLDebug
                    Comando3.Connection = cn
                    dsDatos2.Clear()
                    dsDatos2.Tables.Add(New DataTable)
                    reader1 = Comando3.ExecuteReader
                    bolRead1 = True
                    dsDatos2.Tables(0).Load(reader1)
                    Comando3.Parameters.Clear()
                    If dsDatos2.Tables(0).Rows(0).Item(0) <> 0 Then
                        mExecute = -999
                        bolError = True
                        ReDim strErrores(2)
                        strMensaje = "Trancount <>0 ejecutando:" + strSQL + vbCrLf
                        strMensaje = strMensaje + "FAVOR LLAMAR A SISTEMAS INMEDIATAMENTE"
                        strErrores(0) = strMensaje
                        strErrores(1) = ex.Message
                        strErrores(2) = ex.ToString
                        strEvento = "Error:Trancount Execute:" + strSQL.Replace(vbCrLf, "<enter>") + ":Parametros"
                        For intI = 1 To parametros.Length
                            strEvento = strEvento + ":" + parametros(intI - 1).ParameterName + ":" + parametros(intI - 1).Value.ToString + "<enter>"
                        Next
                        strEvento = strEvento + ":Errores"
                        For intI = 0 To UBound(strErrores, 1)
                            strEvento = strEvento + ":" + strErrores(intI).Replace(vbCrLf, "<enter>")
                        Next
                        If bolRead1 Then
                            reader1.Close()
                            bolRead1 = False
                        End If
                        Throw New System.Exception(strMensaje + vbCrLf + strEvento)
                    End If
                End If
            End Try
        End If
        If bolRead1 Then
            reader1.Close()
            bolRead1 = False
        End If
        If pModo = clsConstantes.ModoTrabajoDAL.ModoDebug And bolError Then
            lngTicks2 = Now.Ticks
            strEvento = "Execute:Error:" + strSQL.Replace(vbCrLf, "<enter>") + ":Parametros"
            For intI = 1 To parametros.Length
                strEvento = strEvento + ":" + parametros(intI - 1).ParameterName + ":" + parametros(intI - 1).Value.ToString + "<enter>"
            Next
            strEvento = strEvento + ":Errores"
            For intI = 0 To UBound(strErrores, 1)
                strEvento = strEvento + ":" + strErrores(intI).Replace(vbCrLf, "<enter>")
            Next
        End If
        If pModo = clsConstantes.ModoTrabajoDAL.ModoDebug And Not bolError Then
            lngTicks2 = Now.Ticks
            strEvento = "Execute:OK:" + strSQL.Replace(vbCrLf, "<enter>") + ":Parametros"
            For intI = 1 To parametros.Length
                strEvento = strEvento + ":" + parametros(intI - 1).ParameterName + ":" + parametros(intI - 1).Value.ToString + "<enter>"
            Next
        End If
    End Function
    Public Function mExecuteQuery(ByVal strSQL As String, Optional ByRef parametros() As System.Data.SqlClient.SqlParameter = Nothing, Optional ByVal CommandTimeout As Integer? = Nothing) As Long
        Dim intI As Integer
        Dim reader1 As System.Data.SqlClient.SqlDataReader = Nothing
        Dim bolRead1 As Boolean
        Dim Comando1 As New System.Data.SqlClient.SqlCommand
        Dim Comando2 As New System.Data.SqlClient.SqlCommand
        Dim Comando3 As New System.Data.SqlClient.SqlCommand
        Dim strSQLDebug As String
        Dim strMensaje As String
        Dim dsDatos2 As New DataSet

        Dim command As SqlCommand
        Dim adapter As New SqlDataAdapter


        If bolRestablecerConexion Then
            Try
                bolRestablecerConexion = False
                mCerrarBasedeDatos()
                mAbrirBasedeDatos(cn.ConnectionString, False)
            Catch ex As Exception
            End Try
        End If

        '    Dim objLogger As New clsLogger
        Dim strEvento As String
        Dim lngTicks1 As Long
        Dim lngTicks2 As Long

        mExecuteQuery = -999
        bolRead1 = False
        'falta commands timeout de los tres command
        lngTicks1 = Now.Ticks
        bolError = False
        ReDim strErrores(0)
        If Not bolAbrio Then
            bolError = True
            strErrores(0) = "La base de datos no esta abierta"
            mExecuteQuery = -999
        Else
            Try

                command = New SqlCommand(strSQL, cn)
                adapter.SelectCommand = command
                adapter.Fill(dsDatos)
                adapter.Dispose()
                command.Dispose()
                mExecuteQuery = -1

                If pModo = clsConstantes.ModoTrabajoDAL.ModoDebug Then
                    strSQLDebug = "select @@trancount as C"
                    Comando2.CommandText = strSQLDebug
                    Comando2.Connection = cn
                    dsDatos2.Clear()
                    dsDatos2.Tables.Add(New DataTable)
                    reader1 = Comando2.ExecuteReader
                    bolRead1 = True
                    dsDatos2.Tables(0).Load(reader1)
                    Comando2.Parameters.Clear()
                    If dsDatos2.Tables(0).Rows(0).Item(0) <> 0 Then
                        mExecuteQuery = -999
                        bolError = True
                        ReDim strErrores(0)
                        strMensaje = "Trancount <>0 ejecutando:" + strSQL + vbCrLf
                        strMensaje = strMensaje + "FAVOR LLAMAR A SISTEMAS INMEDIATAMENTE"
                        strErrores(0) = strMensaje
                        strEvento = "Error:Trancount Execute:" + strSQL.Replace(vbCrLf, "<enter>") + ":Parametros"
                        For intI = 1 To parametros.Length
                            strEvento = strEvento + ":" + parametros(intI - 1).ParameterName + ":" + parametros(intI - 1).Value.ToString + "<enter>"
                        Next
                        strEvento = strEvento + ":Errores"
                        For intI = 0 To UBound(strErrores, 1)
                            strEvento = strEvento + ":" + strErrores(intI).Replace(vbCrLf, "<enter>")
                        Next
                        If bolRead1 Then
                            reader1.Close()
                            bolRead1 = False
                        End If
                        Throw New System.Exception(strMensaje + vbCrLf + strEvento)
                    End If
                End If
            Catch ex As Exception
                Try
                    If TryCast(ex, SqlException).Number = 10054 Then 'Problemas de capa de transporte. Se intentará restablecer la conexión.
                        bolRestablecerConexion = True
                        MsgBox("Problemas de transporte. Por favor contacte al equipo de soporte para recibir instrucciones.", MsgBoxStyle.Exclamation)
                    End If
                Catch ex2 As Exception
                End Try
                bolError = True
                ReDim strErrores(2)
                strErrores(0) = strSQL
                strErrores(1) = ex.Message
                strErrores(2) = ex.ToString
                mExecuteQuery = -999
                If pModo = clsConstantes.ModoTrabajoDAL.ModoDebug Then
                    strSQLDebug = "select @@trancount as C"
                    If bolRead1 Then
                        reader1.Close()
                        bolRead1 = False
                    End If
                    Comando3.CommandText = strSQLDebug
                    Comando3.Connection = cn
                    dsDatos2.Clear()
                    dsDatos2.Tables.Add(New DataTable)
                    reader1 = Comando3.ExecuteReader
                    bolRead1 = True
                    dsDatos2.Tables(0).Load(reader1)
                    Comando3.Parameters.Clear()
                    If dsDatos2.Tables(0).Rows(0).Item(0) <> 0 Then
                        mExecuteQuery = -999
                        bolError = True
                        ReDim strErrores(2)
                        strMensaje = "Trancount <>0 ejecutando:" + strSQL + vbCrLf
                        strMensaje = strMensaje + "FAVOR LLAMAR A SISTEMAS INMEDIATAMENTE"
                        strErrores(0) = strMensaje
                        strErrores(1) = ex.Message
                        strErrores(2) = ex.ToString
                        strEvento = "Error:Trancount Execute:" + strSQL.Replace(vbCrLf, "<enter>") + ":Parametros"
                        For intI = 1 To parametros.Length
                            strEvento = strEvento + ":" + parametros(intI - 1).ParameterName + ":" + parametros(intI - 1).Value.ToString + "<enter>"
                        Next
                        strEvento = strEvento + ":Errores"
                        For intI = 0 To UBound(strErrores, 1)
                            strEvento = strEvento + ":" + strErrores(intI).Replace(vbCrLf, "<enter>")
                        Next
                        If bolRead1 Then
                            reader1.Close()
                            bolRead1 = False
                        End If
                        Throw New System.Exception(strMensaje + vbCrLf + strEvento)
                    End If
                End If
            End Try
        End If
        If bolRead1 Then
            reader1.Close()
            bolRead1 = False
        End If
        If pModo = clsConstantes.ModoTrabajoDAL.ModoDebug And bolError Then
            lngTicks2 = Now.Ticks
            strEvento = "Execute:Error:" + strSQL.Replace(vbCrLf, "<enter>") + ":Parametros"
            For intI = 1 To parametros.Length
                strEvento = strEvento + ":" + parametros(intI - 1).ParameterName + ":" + parametros(intI - 1).Value.ToString + "<enter>"
            Next
            strEvento = strEvento + ":Errores"
            For intI = 0 To UBound(strErrores, 1)
                strEvento = strEvento + ":" + strErrores(intI).Replace(vbCrLf, "<enter>")
            Next
        End If
        If pModo = clsConstantes.ModoTrabajoDAL.ModoDebug And Not bolError Then
            lngTicks2 = Now.Ticks
            strEvento = "Execute:OK:" + strSQL.Replace(vbCrLf, "<enter>") + ":Parametros"
            For intI = 1 To parametros.Length
                strEvento = strEvento + ":" + parametros(intI - 1).ParameterName + ":" + parametros(intI - 1).Value.ToString + "<enter>"
            Next
        End If
    End Function

    Public Function GetDataSet() As DataSet
        GetDataSet = dsDatos
    End Function

End Class

