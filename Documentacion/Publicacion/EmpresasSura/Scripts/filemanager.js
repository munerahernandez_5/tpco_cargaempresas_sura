﻿function SaveAsFile(arrayBuffer, fileName, mimeType) {
    saveAs(new Blob([arrayBuffer], { type: mimeType }), fileName);
}

function showModal(response) {
    if (response.rCode == 0) {
        $('#header-modal').removeClass("danger");
        $('#header-modal').addClass("success");
        $('#header-modal-text').html('<i class="fa fa-thumbs-up"></i> Exito');
    } else {
        $('#header-modal').removeClass("success");
        $('#header-modal').addClass("danger");
        $('#header-modal-text').html('<i class="fa fa-thumbs-down"></i> Error');
    }

    $('.msj-modal').text(response.rDescription);
    $('#myModal').modal('show');
}

function disabledButton() {
    $('.icon-loading').css('display', 'inherit');
    $('#btnloadhistory').attr('disabled', 'disabled');
}

function enabledButton() {
    $('.icon-loading').css('display', 'none');
    $('#btnloadhistory').removeAttr("disabled");
}

function buildRequest() {
    var formdata = new FormData();
    var inputGroupFile01 = document.getElementById('inputGroupFile01');

    formdata.append(inputGroupFile01.name, inputGroupFile01.files[0]);
    return formdata;


}

function resetForm() {
    document.getElementById("formPrestadoras").reset();
    document.getElementById('lblfileName').innerHTML = 'Elija el archivo...';
    $('.msj-validation').css('display', 'none');
    enabledButton();
}