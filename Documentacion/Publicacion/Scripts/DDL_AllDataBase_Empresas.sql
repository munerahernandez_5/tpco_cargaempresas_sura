
/****** Object:  Table [dbo].[tbl_Empresas]    Script Date: 4/08/2021 10:13:54 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbl_Empresas](
	[strNIT] [varchar](15) NOT NULL,
	[strNombre] [varchar](100) NOT NULL,
	[strTipoEmpresa] [varchar](20) NOT NULL,
	[strProfesional] [varchar](100) NOT NULL,
	[strEmail] [varchar](100) NOT NULL,
 CONSTRAINT [PK_tbl_Empresas] PRIMARY KEY CLUSTERED 
(
	[strNIT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Juan Camilo Munera>
-- Create date: <2021-08-03>
-- Description:	<Deletes a Empresas record>
-- =============================================
CREATE PROCEDURE [dbo].[sp_del_Empresas] 
	 @OUT_intError AS INT OUTPUT,
	@OUT_strError AS VARCHAR(MAX) OUTPUT
AS
BEGIN
	SET @OUT_intError = 0
    SET @OUT_strError = ''
	BEGIN TRY
		BEGIN TRANSACTION			
			TRUNCATE TABLE [dbo].[tbl_Empresas]

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
        SET @OUT_intError = ERROR_NUMBER();
        SET @OUT_strError = ERROR_MESSAGE();
	END CATCH
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Juan Camilo Munera>
-- Create date: <2021-08-05>
-- Description:	<Get Info Empreseas>
-- =============================================
CREATE PROCEDURE [dbo].[sp_sel_Empresas] 
	@Documento AS VARCHAR(15),
	@OUT_intError AS INT OUTPUT,
	@OUT_strError AS VARCHAR(MAX) OUTPUT
AS
BEGIN
	SET @OUT_intError = 0
    SET @OUT_strError = ''
	BEGIN TRY
		SELECT E.strNIT AS 'NIT',
		E.strNombre AS 'Nombre',
		E.strTipoEmpresa AS 'TipoEmpresa',
		E.strProfesional AS 'Profesional',
		E.strEmail AS 'Email'
		FROM tbl_Empresas E WHERE E.strNIT=@Documento


	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
        SET @OUT_intError = ERROR_NUMBER();
        SET @OUT_strError = ERROR_MESSAGE();
	END CATCH

END

