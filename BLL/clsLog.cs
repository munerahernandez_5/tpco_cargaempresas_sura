﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public enum FEventType
    {
        Info = 0,
        Alert = 1,
        Exception = 3
    }
    public class clsEvent
    {
        public DateTime TimeStamp;
        public FEventType EventType;
        public string Description;
        public string Error;
        public string StackTrace;
        public string Image;
        public string User;

        public clsEvent(FEventType Type, string Description, Exception Ex, string Image, string User)
        {
            TimeStamp = DateTime.Now;
            EventType = Type;
            this.Description = Description;
            if (Ex != null)
            {
                this.Error = Ex.Message;
                if (Ex.StackTrace != null)
                {
                    this.StackTrace = Ex.StackTrace;
                }
            }
            this.Image = Image;
            this.User = User;
        }
    }

    public class clsLog
    {
        public string FullPath;
        public List<clsEvent> Events;
        public clsLog(string IN_FullPath)
        {
            FullPath = IN_FullPath;
            Events = new List<clsEvent>();
        }
        public void RegisterEvent(FEventType Type, string Description, Exception Ex, Image Image, string User)
        {
            clsEvent NewEvent; //= new clsEvent(Type, Description, Ex, ImageToBase64String(Image, ImageFormat.Jpeg), User);
            if (Image != null)
            {
                NewEvent = new clsEvent(Type, Description, Ex, ImageToBase64String(Image, ImageFormat.Jpeg), User);
            }
            else
            {
                NewEvent = new clsEvent(Type, Description, Ex, null, User);
            }
            Events.Add(NewEvent);
            Save();
        }
        public static clsLog Load(string JsonLog)
        {
            try
            {
                clsLog log = new clsLog(null);
                log = (clsLog)JsonConvert.DeserializeObject(JsonLog, log.GetType());
                return log;
            }
            catch
            {
                return null;
            }
        }
        public void Save()
        {
            System.IO.StreamWriter SW = new System.IO.StreamWriter(FullPath);
            try
            {
                SW.WriteLine(JsonConvert.SerializeObject(this));
                SW.Close();
            }
            catch { }
            finally
            {
                if (SW != null)
                {
                    SW.Dispose();
                }
            }
        }
        // Convert an image to a Base64 string by using a MemoryStream. Save the
        // image to the MemoryStream and use Convert.ToBase64String to convert
        // the content of that MemoryStream to a Base64 string.
        public static string ImageToBase64String(Image Image, ImageFormat ImageFormat)
        {
            // Nivel de compresion
            ImageCodecInfo JpgEncoder = GetEncoder(ImageFormat);
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            EncoderParameters myEncoderParameters = new EncoderParameters(1);
            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 65L);
            myEncoderParameters.Param[0] = myEncoderParameter;
            // *******************
            using (MemoryStream memStream = new MemoryStream())
            {
                Image.Save(memStream, JpgEncoder, myEncoderParameters);
                // Image.Save(AppFullPath() & DirSeparator & LogDirName & DirSeparator & Guid.NewGuid.ToString & ".jpeg", JpgEncoder, myEncoderParameters)
                string result = Convert.ToBase64String(memStream.ToArray());
                memStream.Close();
                return result;
            }
        }

        // Convert a Base64 string back to an image. Fill a MemorySTream based
        // on the Base64 string and call the Image.FromStream() methode to
        // convert the content of the MemoryStream to an image.
        public static Image ImageFromBase64String(string base64)
        {
            using (MemoryStream memStream = new MemoryStream(Convert.FromBase64String(base64)))
            {
                Image result = Image.FromStream(memStream);
                memStream.Close();
                return result;
            }
        }
        public static ImageCodecInfo GetEncoder(ImageFormat Format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (var codec in codecs)
            {
                if (codec.FormatID == Format.Guid)
                    return codec;
            }
            return null;
        }
    }
}
