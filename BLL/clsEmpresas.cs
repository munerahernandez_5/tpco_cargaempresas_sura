﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class clsEmpresas
    {
        public string NIT { get; set; }
        public string Nombre { get; set; }

        public string TipoEmpresa { get; set; }

        public string Profesional { get; set; }

        public string Email { get; set; }

        public static clsEmpresas GetEmpresas(string Documento, string connection, bool encrypted)
        {
            DAL.Database.StoredProcedure sp = DAL.Database.Sps.sp_sel_Empresas(Documento, null, null);
            DataSet ds = null;
            try
            {
                ds = clsUtils.GetDataSetSP(connection, encrypted, ref sp);
            }
            catch
            {
                return null;
            }
            clsEmpresas Empresas = clsUtils.CargarObjeto(typeof(clsEmpresas), ds.Tables[0]) as clsEmpresas;
            return Empresas;
        }


    }
}
