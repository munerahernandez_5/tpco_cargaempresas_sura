﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;
//using Teleperformance.Helpers.Logger;
using Newtonsoft.Json;

namespace BLL
{
    public class clsUtils
    {

        private enum mensaje
        {
            CategoriaIWD = 0,
            FidelidadIWD = 1,
            NombresIWD = 2,
            NumCasoIWD = 3,
            NumSocioIWD = 4,
            OpcionIWD = 5,
            ClaveIWD = 6,
            FabricaIWD = 7,
            Login = 8,
            ServicioOSIWD = 9,
            EpaIWD = 10,
            TrackingIWD = 11,
            //AwbIWD=12,
            ConnID = 12
        }

        public static void Log(object fatal, string v)
        {
            throw new NotImplementedException();
        }

        private enum ClaveIWD
        {
            Mercado = 0,
            Idioma = 1,
            Opcion_URA = 2
        }


        public static void AsignarValor(ref object Objeto, string Propiedad, object Valor)
        {
            try
            {
                string DataTypeInfo = null;
                DataTypeInfo = Objeto.GetType().GetProperty(Propiedad).PropertyType.FullName;

                if (DataTypeInfo.Contains("Int32"))
                {
                    Objeto.GetType().GetProperty(Propiedad).SetValue(Objeto, Convert.ToInt32(Valor), null);
                }
                else if (DataTypeInfo.Contains("String"))
                {
                    Objeto.GetType().GetProperty(Propiedad).SetValue(Objeto, Convert.ToString(Valor), null);
                }
                else if (DataTypeInfo.Contains("Boolean"))
                {
                    Objeto.GetType().GetProperty(Propiedad).SetValue(Objeto, Convert.ToBoolean(Valor), null);
                }
                else if (DataTypeInfo.Contains("DateTime"))
                {
                    Objeto.GetType().GetProperty(Propiedad).SetValue(Objeto, Convert.ToDateTime(Valor.ToString()), null);
                }
                else if (DataTypeInfo.Contains("Double"))
                {
                    Objeto.GetType().GetProperty(Propiedad).SetValue(Objeto, Convert.ToDouble(Valor), null);
                }
                else if (DataTypeInfo.Contains("Decimal"))
                {
                    Objeto.GetType().GetProperty(Propiedad).SetValue(Objeto, Convert.ToDecimal(Valor), null);
                }
                else if (DataTypeInfo.Contains("Guid"))
                {
                    Objeto.GetType().GetProperty(Propiedad).SetValue(Objeto, new Guid(Convert.ToString(Valor)), null);
                }
                else if (DataTypeInfo.Contains("Int64"))
                {
                    Objeto.GetType().GetProperty(Propiedad).SetValue(Objeto, Convert.ToInt64(Valor), null);
                }
                else if (DataTypeInfo.Contains("Long"))
                {
                    Objeto.GetType().GetProperty(Propiedad).SetValue(Objeto, Convert.ToInt64(Valor), null);
                }
                else
                {
                    Objeto.GetType().GetProperty(Propiedad).SetValue(Objeto, Convert.ToString(Valor), null);
                }
            }
            catch (Exception) { }
        }

        public static object CargarObjeto(Type T, DataTable DT)
        {
            if (DT == null)
                return null;
            if (DT.Rows.Count == 0)
                return null;
            object Obj = Activator.CreateInstance(T);
            for (int I = 0; I <= DT.Rows.Count - 1; I++)
            {
                for (int C = 0; C <= DT.Columns.Count - 1; C++)
                {
                    if (!Information.IsDBNull(DT.Rows[I][DT.Columns[C].ColumnName]))
                    {
                        AsignarValor(ref Obj, DT.Columns[C].ColumnName, DT.Rows[I][DT.Columns[C].ColumnName]);
                    }
                }
            }
            return Obj;
        }

        public static List<object> CargarObjetos(Type T, DataTable DT)
        {
            if (DT == null)
                return null;
            if (DT.Rows.Count == 0)
                return null;
            List<object> ListObj = new List<object>();
            object Obj = null;
            foreach (DataRow Fila in DT.Rows)
            {
                Obj = Activator.CreateInstance(T);
                foreach (DataColumn Col in DT.Columns)
                {
                    if (!Information.IsDBNull(Fila[Col.ColumnName]))
                    {
                        AsignarValor(ref Obj, Col.ColumnName, Fila[Col.ColumnName]);
                    }
                }
                ListObj.Add(Obj);
            }
            return ListObj;
        }

        /// <summary>
        /// Devuelve un DataSet con lo que retorne el SP
        /// </summary>
        /// <param name="SP">
        /// Procedimiento almacenado al cual se le aplicará el GetDataSet
        /// </param>
        /// <param name="OutputValues">
        /// Parámetro por referencia opcional, si se pasa, en su interior contendrá todos parámetros de salida del sp
        /// </param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static DataSet GetDataSetSP(string ConnectionString, bool EsCifrada, ref DAL.Database.StoredProcedure SP, ref List<object> OutputValues)
        {
            int OUT_intError = 0;
            string OUT_strError = string.Empty;
            DataSet DS = new DataSet();
            try
            {
                DS = SP.GetDataSet(ConnectionString, EsCifrada);
                for (int I = 0; I <= SP.OutputValues.Count - 1; I++)
                {
                    switch (((System.Data.SqlClient.SqlParameter)SP.OutputValues[I]).ParameterName)
                    {
                        case "@OUT_intError":
                            OUT_intError = Convert.ToInt32(((System.Data.SqlClient.SqlParameter)SP.OutputValues[I]).Value);
                            break;
                        case "@OUT_strError":
                            OUT_strError = Convert.ToString(((System.Data.SqlClient.SqlParameter)SP.OutputValues[I]).Value);
                            break;
                    }
                }

                if ((OutputValues != null))
                {
                    OutputValues = new List<object>(SP.OutputValues.ToArray());
                }
                if (OUT_intError != 0)
                {
                    throw new Exception("Código de error: " + Convert.ToString(OUT_intError) + Constants.vbCrLf + "Mensaje de error: " + OUT_strError);
                }
                SP.Dispose();
                if (DS == null)
                    return null;
                if (DS.Tables.Count == 0)
                    return null;
                return DS;
            }
            finally
            {
                if ((DS != null))
                    DS.Dispose();
                SP.Dispose();
            }
        }

        /// <summary>
        /// Devuelve un DataSet con lo que retorne el SP
        /// </summary>
        /// <param name="SP">
        /// Procedimiento almacenado al cual se le aplicará el GetDataSet
        /// </param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static DataSet GetDataSetSP(string ConnectionString, bool EsCifrada, ref DAL.Database.StoredProcedure SP)
        {
            int OUT_intError = 0;
            string OUT_strError = string.Empty;
            DataSet DS = new DataSet();
            try
            {
                DS = SP.GetDataSet(ConnectionString, EsCifrada);
                for (int I = 0; I <= SP.OutputValues.Count - 1; I++)
                {
                    switch (((System.Data.SqlClient.SqlParameter)SP.OutputValues[I]).ParameterName)
                    {
                        case "@OUT_intError":
                            OUT_intError = Convert.ToInt32(((System.Data.SqlClient.SqlParameter)SP.OutputValues[I]).Value);
                            break;
                        case "@OUT_strError":
                            OUT_strError = Convert.ToString(((System.Data.SqlClient.SqlParameter)SP.OutputValues[I]).Value);
                            break;
                    }
                }

                if (OUT_intError != 0)
                {
                    throw new Exception("Código de error: " + Convert.ToString(OUT_intError) + Constants.vbCrLf + "Mensaje de error: " + OUT_strError);
                }
                SP.Dispose();
                if (DS == null)
                    return null;
                if (DS.Tables.Count == 0)
                    return null;
                return DS;
            }
            finally
            {
                if ((DS != null))
                    DS.Dispose();
                SP.Dispose();
            }
        }

        /// <summary>
        /// Ejecuta un SP
        /// </summary>
        /// <param name="SP">
        /// Procedimiento almacenado al cual se le aplicará el GetDataSet
        /// </param>
        /// <param name="OutputValues">
        /// Parámetro por referencia opcional, si se pasa, en su interior contendrá todos parámetros de salida del sp
        /// </param>
        /// <remarks></remarks>
        public static void ExecuteSP(string ConnectionString, bool EsCifrada, ref DAL.Database.StoredProcedure SP, int? CommandTimeout, ref List<object> OutputValues)
        {
            int OUT_intError = 0;
            string OUT_strError = string.Empty;
            SP.Execute(ConnectionString, EsCifrada, CommandTimeout);
            for (int I = 0; I <= SP.OutputValues.Count - 1; I++)
            {
                switch (((System.Data.SqlClient.SqlParameter)SP.OutputValues[I]).ParameterName)
                {
                    case "@OUT_intError":
                        OUT_intError = Convert.ToInt32(((System.Data.SqlClient.SqlParameter)SP.OutputValues[I]).Value);
                        break;
                    case "@OUT_strError":
                        OUT_strError = Convert.ToString(((System.Data.SqlClient.SqlParameter)SP.OutputValues[I]).Value);
                        break;
                }
            }
            if ((OutputValues != null))
            {
                OutputValues = new List<object>(SP.OutputValues.ToArray());
            }
            if (OUT_intError != 0)
            {
                throw new Exception("Código de error: " + Convert.ToString(OUT_intError) + Constants.vbCrLf + "Mensaje de error: " + OUT_strError);
            }
            SP.Dispose();
        }

        /// <summary>
        /// Ejecuta un SP
        /// </summary>
        /// <param name="SP">
        /// Procedimiento almacenado al cual se le aplicará el GetDataSet
        /// </param>
        /// <remarks></remarks>
        public static void ExecuteSP(string ConnectionString, bool EsCifrada, ref DAL.Database.StoredProcedure SP, int? CommandTimeout)
        {
            int OUT_intError = 0;
            string OUT_strError = string.Empty;
            SP.Execute(ConnectionString, EsCifrada, CommandTimeout);
            for (int I = 0; I <= SP.OutputValues.Count - 1; I++)
            {
                switch (((System.Data.SqlClient.SqlParameter)SP.OutputValues[I]).ParameterName)
                {
                    case "@OUT_intError":
                        OUT_intError = Convert.ToInt32(((System.Data.SqlClient.SqlParameter)SP.OutputValues[I]).Value);
                        break;
                    case "@OUT_strError":
                        OUT_strError = Convert.ToString(((System.Data.SqlClient.SqlParameter)SP.OutputValues[I]).Value);
                        break;
                }
            }
            if (OUT_intError != 0)
            {
                throw new Exception("Código de error: " + Convert.ToString(OUT_intError) + Constants.vbCrLf + "Mensaje de error: " + OUT_strError);
            }
            SP.Dispose();
        }

        public static string ReplaceQueryParams(string Query, string Splits, string VDNs, DateTime? FechaEjecucion, DateTime? FechaUltimaEjecucion)
        {
            DateTime FechaActual = DateTime.Now.Date;
            DateTime FechaAyer = FechaActual.AddDays(-1);
            if (FechaEjecucion == null)
                FechaEjecucion = FechaActual;

            if (FechaUltimaEjecucion == null)
                FechaUltimaEjecucion = FechaAyer;

            if (Query.Contains("@{SPLIT}"))
                Query = Query.Replace("@{SPLIT}", Splits);
            if (Query.Contains("@{VDN}"))
                Query = Query.Replace("@{VDN}", VDNs);

            if (Query.Contains("@{Día Actual [DD]}"))
                Query = Query.Replace("@{Día Actual [DD]}", FechaActual.Day.ToString("00"));
            if (Query.Contains("@{Mes Actual [MM]}"))
                Query = Query.Replace("@{Mes Actual [MM]}", FechaActual.Month.ToString("00"));
            if (Query.Contains("@{Año Actual [YYYY]}"))
                Query = Query.Replace("@{Año Actual [YYYY]}", FechaActual.Year.ToString("0000"));
            if (Query.Contains("@{Hora Actual [HH]}"))
                Query = Query.Replace("@{Hora Actual [HH]}", FechaActual.Hour.ToString("00"));
            if (Query.Contains("@{Minuto Actual [mm]}"))
                Query = Query.Replace("@{Minuto Actual [mm]}", FechaActual.Minute.ToString("00"));
            if (Query.Contains("@{Segundo Actual [SS]}"))
                Query = Query.Replace("@{Segundo Actual [SS]}", FechaActual.Second.ToString("00"));

            if (Query.Contains("@{Día Ayer [DD]}"))
                Query = Query.Replace("@{Día Ayer [DD]}", FechaAyer.Day.ToString("00"));
            if (Query.Contains("@{Mes Ayer [MM]}"))
                Query = Query.Replace("@{Mes Ayer [MM]}", FechaAyer.Month.ToString("00"));
            if (Query.Contains("@{Año Ayer [YYYY]}"))
                Query = Query.Replace("@{Año Ayer [YYYY]}", FechaAyer.Year.ToString("0000"));

            if (Query.Contains("@{Día Ejecución [DD]}"))
                Query = Query.Replace("@{Día Ejecución [DD]}", FechaEjecucion.Value.Day.ToString("00"));
            if (Query.Contains("@{Mes Ejecución [MM]}"))
                Query = Query.Replace("@{Mes Ejecución [MM]}", FechaEjecucion.Value.Month.ToString("00"));
            if (Query.Contains("@{Año Ejecución [YYYY]}"))
                Query = Query.Replace("@{Año Ejecución [YYYY]}", FechaEjecucion.Value.Year.ToString("0000"));
            if (Query.Contains("@{Hora Ejecución [HH]}"))
                Query = Query.Replace("@{Hora Ejecución [HH]}", FechaEjecucion.Value.Hour.ToString("00"));
            if (Query.Contains("@{Minuto Ejecución [mm]}"))
                Query = Query.Replace("@{Minuto Ejecución [mm]}", FechaEjecucion.Value.Minute.ToString("00"));
            if (Query.Contains("@{Segundo Ejecución [SS]}"))
                Query = Query.Replace("@{Segundo Ejecución [SS]}", FechaEjecucion.Value.Second.ToString("00"));

            if (Query.Contains("@{Día Última Ejecución [DD]}"))
                Query = Query.Replace("@{Día Última Ejecución [DD]}", FechaUltimaEjecucion.Value.Day.ToString("00"));
            if (Query.Contains("@{Mes Última Ejecución [MM]}"))
                Query = Query.Replace("@{Mes Última Ejecución [MM]}", FechaUltimaEjecucion.Value.Month.ToString("00"));
            if (Query.Contains("@{Año Última Ejecución [YYYY]}"))
                Query = Query.Replace("@{Año Última Ejecución [YYYY]}", FechaUltimaEjecucion.Value.Year.ToString("0000"));
            if (Query.Contains("@{Hora Última Ejecución [HH]}"))
                Query = Query.Replace("@{Hora Última Ejecución [HH]}", FechaUltimaEjecucion.Value.Hour.ToString("00"));
            if (Query.Contains("@{Minuto Última Ejecución [mm]}"))
                Query = Query.Replace("@{Minuto Última Ejecución [mm]}", FechaUltimaEjecucion.Value.Minute.ToString("00"));
            if (Query.Contains("@{Segundo Última Ejecución [SS]}"))
                Query = Query.Replace("@{Segundo Última Ejecución [SS]}", FechaUltimaEjecucion.Value.Second.ToString("00"));

            return Query;
        }

        public class LOG
        {
            public DateTime EventDate { get; set; }
            public string Message { get; set; }
            public string InnerException { get; set; }
            public string StackTrace { get; set; }
        }
        static List<LOG> EventLog = new List<LOG>();
        static string LogPath;
        public static void CreateLogsFolder(string ExecutablePath)
        {
            LogPath = Path.GetDirectoryName(ExecutablePath);
            LogPath = Path.Combine(LogPath, "LOGs");
            if (!Directory.Exists(LogPath))
            {
                Directory.CreateDirectory(LogPath);
            }
        }
        public static void SaveLog(Exception ex)
        {
            LOG Event = new LOG();
            Event.EventDate = DateTime.Now;
            Event.Message = ex.Message;
            if (ex.InnerException != null)
            {
                Event.InnerException = ex.InnerException.Message;
            }
            Event.StackTrace = ex.StackTrace;
            EventLog.Add(Event);

            XmlSerializer serializer = new XmlSerializer(typeof(List<LOG>));
            using (FileStream stream = File.OpenWrite(Path.Combine(LogPath, "LOG_" + DateTime.Now.ToString("dd_MM_yyyy") + ".log")))
            {
                serializer.Serialize(stream, EventLog);
            }
        }


        public static bool MensajeLog(string nombreMetodo, double tiempo, object request, object response, object error)
        {
            bool result = false;
            string sRequest = string.Empty;
            string sResponse = string.Empty;
            //string logtag = LogTags.TRACE;

            try
            {
                //if (ConfigurationManager.AppSettings["LogRequest"] != "S")
                //{
                //    request = null;
                //}

                //if (ConfigurationManager.AppSettings["LogResponse"] != "S")
                //{
                //    response = null;
                //}

                if (error != null)
                {
                   // logtag = LogTags.ERROR;
                }

                string msg = string.Format("Nombre método: {0} - Tiempo (ms): {1} - request: {2} - response: {3} - error:{4}", nombreMetodo, tiempo, JsonConvert.SerializeObject(request), JsonConvert.SerializeObject(response), JsonConvert.SerializeObject(error));
                //LoggerManager.Logger.WriteMessage(LogLevel.Debug, logtag, msg);
                result = true;
            }
            catch (Exception)
            {
                //LoggerManager.Logger.WriteMessage(LogLevel.Debug, LogTags.ERROR, "No se pudo hacer el log");
            }
            return result;
        }


        public static DataSet mRST(string ConnectionString, bool EsCifrada, string strSQL, System.Data.SqlClient.SqlParameter[] parametros = null)
        {
            DataSet result = null;

            clsDAL dataAccess = new clsDAL();
            dataAccess.mAbrirBasedeDatos(ConnectionString, EsCifrada);
            long resulExecute = dataAccess.mExecuteQuery(strSQL, ref parametros,null);
            if (resulExecute != -999)
            {
                result = dataAccess.GetDataSet();

            }
            dataAccess.mCerrarBasedeDatos();
            dataAccess = null;
            return result;

        }
    }
}
