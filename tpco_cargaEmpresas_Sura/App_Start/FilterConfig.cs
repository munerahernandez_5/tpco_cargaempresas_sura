﻿using System.Web;
using System.Web.Mvc;

namespace tpco_cargaEmpresas_Sura
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
