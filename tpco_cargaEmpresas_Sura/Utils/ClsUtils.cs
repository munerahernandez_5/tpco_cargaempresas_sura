﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using tpco_cargaEmpresas_Sura.DataAccess;
using tpco_cargaEmpresas_Sura.Models;
using tpco_cargaEmpresas_Sura.ViewModel;

namespace tpco_cargaEmpresas_Sura.Utils
{
    public class ClsUtils
    {
        public static string strUser = null;

        public static void ExecuteSP(string ConnectionString, bool EsCifrada, ref DAL.Database.StoredProcedure SP, int? CommandTimeout)
        {
            int OUT_intError = 0;
            string OUT_strError = string.Empty;
            SP.Execute(ConnectionString, EsCifrada, CommandTimeout);
            for (int I = 0; I <= SP.OutputValues.Count - 1; I++)
            {
                switch (((System.Data.SqlClient.SqlParameter)SP.OutputValues[I]).ParameterName)
                {
                    case "@OUT_intError":
                        OUT_intError = Convert.ToInt32(((System.Data.SqlClient.SqlParameter)SP.OutputValues[I]).Value);
                        break;
                    case "@OUT_strError":
                        OUT_strError = Convert.ToString(((System.Data.SqlClient.SqlParameter)SP.OutputValues[I]).Value);
                        break;
                }
            }
            if (OUT_intError != 0)
            {
                // [EDIT] Luis Escobar 19-07-26: Formating and english translation
                //throw new Exception("Código de error: " + Convert.ToString(OUT_intError) + Constants.vbCrLf + "Mensaje de error: " + OUT_strError);
                throw new Exception("Error Code: " + Convert.ToString(OUT_intError) + Environment.NewLine + "Message: " + OUT_strError);
            }
            SP.Dispose();
        }

        public static ResponseService ReadFile(FileManager fileManager)
        {
            Empresas empresas;
            ResponseService response = new ResponseService();
            ResponseService responsebd = new ResponseService();
            DataTable dtempresas = new DataTable("Empresas");

           



            try
            {               


                if (fileManager.LoadFile.ContentLength > Services.clsGlobal.ContentLength)
                {
                    response.code = "-1";
                    response.message = "El archivo supera el tamaño maximo permitido";
                    return response;
                }


                if (!fileManager.LoadFile.FileName.EndsWith(Services.clsGlobal.ArchivosPermitidos))
                {
                    response.code = "-1";
                    response.message = "No es un tipo de archivo permitido";
                    return response;
                }


                dtempresas.Columns.Add("strNIT", typeof(String));
                dtempresas.Columns.Add("strNombre", typeof(String));
                dtempresas.Columns.Add("strTipoEmpresa", typeof(String));
                dtempresas.Columns.Add("strProfesional", typeof(String));
                dtempresas.Columns.Add("stremail", typeof(String));
                



                ExcelPackage package = new ExcelPackage(fileManager.LoadFile.InputStream);

                ExcelWorksheet worksheet = package.Workbook.Worksheets.First();
                int noOfCol = worksheet.Dimension.End.Column;
                int noOfRow = worksheet.Dimension.End.Row;

                empresas = new Empresas();

                for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                {


                    try
                    {
                        string strNit = ConvertNumberToString(worksheet.Cells[rowIterator, 1].Value);
                        if (strNit=="")
                        {
                            int row = rowIterator - 1;
                            response.code = "-1";
                            response.message = "No se puede cargar la fila: " + row + " porque el Nit está erroneo.";
                            return response;
                        }

                        if (strNit.Length>25)
                        {
                            int row = rowIterator - 1;
                            response.code = "-1";
                            response.message = "No se puede cargar la fila: " + row + " porque el Nit tiene una longitud mayor a 15.";
                            return response;
                        }


                        string strNombre = ConvertNumberToString(worksheet.Cells[rowIterator, 2].Value);
                        if (strNombre == "")
                        {
                            int row = rowIterator - 1;
                            response.code = "-1";
                            response.message = "No se puede cargar la fila: " + row + " porque el Nombre está erroneo.";
                            return response;
                        }


                        if (strNombre.Length > 100)
                        {
                            int row = rowIterator - 1;
                            response.code = "-1";
                            response.message = "No se puede cargar la fila: " + row + " porque el Nombre tiene una longitud mayor a 100.";
                            return response;
                        }


                        string strTipoEmpresa = ConvertNumberToString(worksheet.Cells[rowIterator, 3].Value);
                        if (strTipoEmpresa == "")
                        {
                            int row = rowIterator - 1;
                            response.code = "-1";
                            response.message = "No se puede cargar la fila: " + row + " porque el Tipo Empresa está erroneo.";
                            return response;
                        }

                        if (strTipoEmpresa.Length >20)
                        {
                            int row = rowIterator - 1;
                            response.code = "-1";
                            response.message = "No se puede cargar la fila: " + row + " porque TipoEmpresa tiene una longitud mayor a 20.";
                            return response;
                        }

                        string strProfesional = ConvertNumberToString(worksheet.Cells[rowIterator, 4].Value);
                        if (strProfesional == "")
                        {
                            int row = rowIterator - 1;
                            response.code = "-1";
                            response.message = "No se puede cargar la fila: " + row + " porque el Profesional está erroneo.";
                            return response;
                        }

                        if (strProfesional.Length >100)
                        {
                            int row = rowIterator - 1;
                            response.code = "-1";
                            response.message = "No se puede cargar la fila: " + row + " porque Profesional tiene una longitud mayor a 100.";
                            return response;
                        }

                        string strEmail = ConvertNumberToString(worksheet.Cells[rowIterator, 5].Value);

                        if (strEmail == "")
                        {
                            int row = rowIterator - 1;
                            response.code = "-1";
                            response.message = "No se puede cargar la fila: " + row + " porque el Email está erroneo.";
                            return response;
                        }


                        if (strEmail.Length >100)
                        {
                            int row = rowIterator - 1;
                            response.code = "-1";
                            response.message = "No se puede cargar la fila: " + row + " porque Email tiene una longitud mayor a 100.";
                            return response;
                        }


                        empresas.strNIT = strNit;
                        empresas.strNombre = strNombre;
                        empresas.strTipoEmpresa = strTipoEmpresa;
                        empresas.strProfesional = strProfesional;
                        empresas.strEmail = strEmail;
                        


                        DataRow _row = dtempresas.NewRow();
                        _row["strNIT"] = empresas.strNIT;
                        _row["strNombre"] = empresas.strNombre;
                        _row["strTipoEmpresa"] = empresas.strTipoEmpresa;
                        _row["strProfesional"] = empresas.strProfesional;
                        _row["strEmail"] = empresas.strEmail;
                        dtempresas.Rows.Add(_row);
                    }
                    catch (Exception ex)
                    {
                        int row = rowIterator - 1;
                        response.code = "-1";
                        response.message = "No se puede cargar la fila: " + row + " porque hay un dato vacio que es obligatorio.";
                        return response;
                    }
                }

                DB.Delete(empresas);
                responsebd = DataAccess.Data.InsertFilePrestadoras(dtempresas,Services.clsGlobal.dns);
                response.code = responsebd.code;
                response.message = responsebd.message;
                return response;
            }
            catch (Exception ex)
            {
                response.code = "-1";
                response.message = "Error en la aplicacion al cargar el archivo" + " " + ex.Message;
                return response;
            }
        }

        public static string ConvertNumberToString(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(String))
                {
                    return obj.ToString();
                }

                if (obj.GetType() == typeof(Int32))
                {
                    return Convert.ToInt32(obj).ToString();
                }

                if (obj.GetType() == typeof(Double))
                {
                    return Convert.ToDouble(obj).ToString();
                }

                if (obj.GetType() == typeof(Decimal))
                {
                    return Convert.ToDouble(obj).ToString();
                }

                return "";
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}