﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tpco_cargaEmpresas_Sura.Models;
using tpco_cargaEmpresas_Sura.Utils;

namespace tpco_cargaEmpresas_Sura.Controllers
{
    public class AdmonEmpresasController : Controller
    {
        // GET: AdmonEmpresas
        public ActionResult Index()
        {
            return View();
        }
        

        // GET: AdmonEmpresas/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }


        [HttpPost]
        public JsonResult LoadFile(FileManager fileManager)
        {            
           return Json(ClsUtils.ReadFile(fileManager), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public ActionResult DownloadStructure()
        {
            ClsUtils utils = new ClsUtils();
            DataTable dtEmpresas = new DataTable("Empresas");
            dtEmpresas.Columns.Add("NIT", typeof(String));
            dtEmpresas.Columns.Add("Nombre", typeof(String));
            dtEmpresas.Columns.Add("Tipo Empresa", typeof(String));
            dtEmpresas.Columns.Add("Profesional", typeof(String));
            dtEmpresas.Columns.Add("Email", typeof(String));          


            FileContentResult robj;
            //create a new ExcelPackage
            using (ExcelPackage excelPackage = new ExcelPackage())
            {
                //create a WorkSheet
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Sheet 1");

                //add all the content from the DataTable, starting at cell A1
                worksheet.Cells["A1"].LoadFromDataTable(dtEmpresas, true);


                for (int row = 1; row <= worksheet.Dimension.End.Row; row++)
                {
                    var re = worksheet.Cells[row, 4].Value.ToString();
                    int value;
                    if (int.TryParse(worksheet.Cells[row, 4].Value.ToString().Split('%')[0], out value))
                    {
                        if (value <= 80 && value >= 70)
                        {
                            worksheet.Cells[row, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        }
                        else if (value < 70)
                        {
                            worksheet.Cells[row, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        }

                    }

                }
                using (MemoryStream stream = new MemoryStream())
                {
                    excelPackage.SaveAs(stream);
                    var bytesdata = File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "myFileName.xlsx");
                    robj = bytesdata;
                }

            }

            return Json(robj, JsonRequestBehavior.AllowGet);

        }


        // GET: AdmonEmpresas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdmonEmpresas/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AdmonEmpresas/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: AdmonEmpresas/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AdmonEmpresas/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: AdmonEmpresas/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
