﻿using DAL.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tpco_cargaEmpresas_Sura.Utils;

namespace tpco_cargaEmpresas_Sura.DataAccess
{
    public class DB
    {
        public struct ModelTypes
        {
            public const string Empresas = "tpco_cargaEmpresas_Sura.ViewModel.Empresas";
           

        }

      

        public static void Delete(object obj)
        {
            int OUT_intError = 0;
            string OUT_strError = "";
            var typeName = obj.GetType().ToString();
            StoredProcedure sp = null;

            switch (typeName)
            {
                case ModelTypes.Empresas:
                    sp = Sps.sp_del_Empresas(OUT_intError, OUT_strError);
                    break;
            }

            ClsUtils.ExecuteSP(Services.clsGlobal.dns, false, ref sp, null);
            if (OUT_intError != 0) { throw new Exception(OUT_strError); }
        }
    }
}