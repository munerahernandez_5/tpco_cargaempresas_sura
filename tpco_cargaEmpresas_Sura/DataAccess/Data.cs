﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using tpco_cargaEmpresas_Sura.Models;

namespace tpco_cargaEmpresas_Sura.DataAccess
{
    public class Data
    {
        public static ResponseService InsertFilePrestadoras(DataTable dt, string ConnectionString)
        {
            ResponseService response = new ResponseService();
            try
            {
                using (var sqlBulk = new SqlBulkCopy(ConnectionString))
                {
                    sqlBulk.BulkCopyTimeout = 0;
                    sqlBulk.DestinationTableName = "tbl_Empresas";
                    sqlBulk.WriteToServer(dt);
                    response.code = "0";
                    response.message = "Se cargo el archivo exitosamente";
                }
            }
            catch (Exception ex)
            {
                response.code = "-1";
                response.message = "Error en la aplicacion al cargar el archivo" + " " + ex.Message;
            }

            return response;
        }
    }
}