﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tpco_cargaEmpresas_Sura.Models
{
    public class FileManager
    {
        [Required]
        public HttpPostedFileBase LoadFile { get; set; }
    }
}