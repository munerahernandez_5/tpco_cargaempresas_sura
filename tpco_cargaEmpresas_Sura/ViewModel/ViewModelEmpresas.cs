﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tpco_cargaEmpresas_Sura.ViewModel
{
    public class ViewModelEmpresas
    {
        public ViewModelEmpresas()
        {

            prestform = new Empresas();
        }

        public Empresas prestform { get; set; }

        public List<Empresas> prestadoras { get; set; }
    }
}