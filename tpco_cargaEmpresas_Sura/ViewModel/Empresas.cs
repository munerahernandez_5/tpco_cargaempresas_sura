﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tpco_cargaEmpresas_Sura.ViewModel
{
    public class Empresas
    {
        public Empresas() { }


        [Display(Name = "NIT")]
        public string strNIT { get; set; }

        [Display(Name = "Nombre")]
        public string strNombre { get; set; }

        [Display(Name = "Tipo Empresa")]
        public string strTipoEmpresa { get; set; }

        [Display(Name = "Profesional")]
        public string strProfesional {get; set; }

        [Display(Name = "Email")]
        public string strEmail { get; set; }


    }
}