﻿using MiddlwareEmpresasSura.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace MiddlwareEmpresasSura.Controllers
{
    public class ConsultaEmpresasController : ApiController
    {


        [Route("api/ConsultaEmpresa/GetEmpresas")]
        [HttpPost]
        public OutPutEmpresas GetEmpresas([FromBody]InputValues Documento)
        {
            OutPutEmpresas Empresa = new OutPutEmpresas();
            BLL.clsEmpresas Data = Services.DB.GetEmpresas(Documento.DocumentoEmpresa);
            if (Data == null)
            {
                Empresa.ResponseCode = "-1";
                Empresa.ResponseDescription = "Data configuration not found";
                return Empresa;
            }

            Empresa.ResponseCode = "1";
            Empresa.ResponseDescription = "ok";
            Empresa.NIT = Data.NIT;
            Empresa.Nombre = Data.Nombre;
            Empresa.TipoEmpresa = Data.TipoEmpresa;
            Empresa.Profesional = Data.Profesional;
            Empresa.Email = Data.Email;

            return Empresa;
        }

        
       
    }
}
