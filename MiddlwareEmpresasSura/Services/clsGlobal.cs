﻿using BLL;
using MiddlwareEmpresasSura.wsTPStrings;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;

namespace MiddlwareEmpresasSura.Services
{
    public class clsGlobal
    {
        static string fdns;
        private static string fIpAddress;
        public static clsLog Log;
        public static string dns
        {
            get { return fdns; }
        }

        public static string IPAddress
        {
            get { return fIpAddress; }
        }

        public static string AppFullPath()
        {
            return AppDomain.CurrentDomain.BaseDirectory;
        }

        public static void DeleteFilesFolderByDate(string DirectoryPath, int DaysToDel)
        {
            DirectoryInfo Source = new DirectoryInfo(DirectoryPath);
            if (Source.Exists)
            {
                foreach (FileInfo FI in Source.GetFiles())
                {
                    DateTime CreationTime = FI.CreationTime;
                    DateTime DeleteTime = DateTime.Now - new TimeSpan(DaysToDel, 0, 0, 0);
                    if (FI.CreationTime < DeleteTime)
                    {
                        FI.Delete();
                    }
                }
            }

        }

        public static void Initialize()
        {
            try
            {
                const string LogDirName = "Log";
                const string LogExt = ".Log";
                const int DaysAgo = 30;

                if (!Directory.Exists(AppFullPath() + Path.DirectorySeparatorChar + LogDirName))
                {
                    Directory.CreateDirectory(AppFullPath() + Path.DirectorySeparatorChar + LogDirName);
                }

                Log = new clsLog(AppFullPath() + LogDirName + Path.DirectorySeparatorChar + Guid.NewGuid().ToString() + LogExt);
                Log.RegisterEvent(FEventType.Info, "Start Application.", null, null, Environment.UserName);
                DeleteFilesFolderByDate(AppFullPath() + Path.DirectorySeparatorChar + LogDirName, DaysAgo);


                string TPStrings = ConfigurationManager.AppSettings["TPStrings"];
                string TPStringsBK = ConfigurationManager.AppSettings["TPStringsBK"];
                string TPStringsUser = ConfigurationManager.AppSettings["TPStringsUser"];
                string TPStringsPassword = ConfigurationManager.AppSettings["TPStringsPassword"];
                string ApplicationId = ConfigurationManager.AppSettings["ApplicationId"];
                string ApplicationName = ConfigurationManager.AppSettings["ApplicationName"];
                fIpAddress = GetLocalIPAddress();

                wsTPStrings1 TPStringsService = new wsTPStrings1();
                TPStringsService.Url = TPStrings;
                ResponseGetParamsByIdloginList TPStringsResponse = new ResponseGetParamsByIdloginList();
            

                Log.RegisterEvent(FEventType.Info, "Connect to TPStrings.", null, null, Environment.UserName);
                try
                {
                    TPStringsResponse = TPStringsService.GetParamsByIdloginList(Convert.ToInt32(ApplicationId), TPStringsUser, TPStringsPassword, ApplicationName, Environment.UserName, TPStringsUser, IPAddress);
                }
                catch (Exception ex)
                {
                    Log.RegisterEvent(FEventType.Exception, "Error in start application GetParamsByIdloginList BK.", ex, null, Environment.UserName);

                    TPStringsService.Url = TPStringsBK;
                    TPStringsResponse = TPStringsService.GetParamsByIdloginList(Convert.ToInt32(ApplicationId), TPStringsUser, TPStringsPassword, ApplicationName, Environment.UserName, TPStringsUser, IPAddress);

                }
                Log.RegisterEvent(FEventType.Info, "End connect to TPStrings.", null, null, Environment.UserName);
                if (TPStringsResponse.EsError)
                {
                    Log.RegisterEvent(FEventType.Exception, "Error in start application. " + TPStringsResponse.Mensaje, null, null, Environment.UserName);
                    return;
                }
                if (TPStringsResponse.Parametros != null)
                {
                    if (TPStringsResponse.Parametros.Length > 0)
                    {
                        for (int I = 0; I <= TPStringsResponse.Parametros.Length - 1; I++)
                        {
                            if (TPStringsResponse.Parametros[I].Parametro == "DSN")
                            {
                                fdns = TPStringsResponse.Parametros[I].Valor;
                            }

                        }
                    }
                }
                else
                {
                    return;
                }


            }
            catch (Exception ex)
            {

                Log.RegisterEvent(FEventType.Exception, "Error in start application.", ex, null, Environment.UserName);



            }
        }

        private static string GetLocalIPAddress()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

    }
}