﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiddlwareEmpresasSura.Services
{
    public class DB
    {
        public static BLL.clsEmpresas GetEmpresas(string Empresas)
        {
            BLL.clsEmpresas result = BLL.clsEmpresas.GetEmpresas(Empresas,clsGlobal.dns, false);
            return result;
        }
    }
}