﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiddlwareEmpresasSura.Entities
{
    public class OutPutEmpresas
    {
        public string ResponseCode { get; set; }

        public string ResponseDescription { get; set; }

        public string NIT { get; set; }
        public string Nombre { get; set; }

        public string TipoEmpresa { get; set; }

        public string Profesional { get; set; }

        public string Email { get; set; }
    }
}